<?php
/**
 * User: chevu
 * Time: 12:15 AM
 */


//defined('MOODLE_INTERNAL') || die();
global $CFG, $OUTPUT, $DB, $PAGE, $USER;

// Obviously required
require_once('../../config.php');
require_once($CFG->dirroot . '/comment/lib.php');
require_once($CFG->dirroot.'/rating/lib.php');

$id = optional_param('id', 0, PARAM_INT);

//get id from url or print error
if (!empty($id)) {
    $params = array('id' => $id);
}else {
    print_error('unspecifycourseid', 'error');
}

//get course from db based on id
$course = $DB->get_record('course', $params, '*', MUST_EXIST);

//set url params
$urlparams = array('id' => $course->id);

$PAGE->set_url('/blocks/commentsandratings/view.php', $urlparams); // Defined here to avoid notices on errors etc

//get course context
context_helper::preload_course($course->id);
$context = context_course::instance($course->id, MUST_EXIST);

require_login($course);

// Get section details and check it exists.
$modinfo = get_fast_modinfo($course);
$sections = $modinfo->get_section_info_all();

// Fix course format if it is no longer installed
$course->format = course_get_format($course)->get_format();

///Get all resources for this course
$resources = get_array_of_activities($course->id);

// Must set layout before gettting section info. See MDL-47555.
$PAGE->set_pagelayout('course');
$PAGE->set_pagetype('course-view-' . $course->format);
$PAGE->set_other_editing_capability('moodle/course:update');
$PAGE->set_other_editing_capability('moodle/course:manageactivities');
$PAGE->set_other_editing_capability('moodle/course:activityvisibility');
$PAGE->set_heading($course->fullname . ' - ' . 'Komentari i Ocene');

echo $OUTPUT->header();

$html = "<div class='course-content'>";
$html .= '<ul class="weeks">'; //ul1

foreach ($sections as $section) {

    if ($section && $section->section > 0) {

//        get section name
        $section_name = get_section_name($course, $section);

        $html .= "<li id='section". $section->id ."' class='section main clearfix' role='aria-label'>"; //li1
        $html .= "<div class='left side'></div>";
        $html .= "<div class='right side'></div>";
        $html .= "<div class='content'>"; // div1
        $html .= "<h3 class='sectionname'>"; //h31
        $html .= "<span><a href='" . $CFG->wwwroot . "/blocks/commentsandratings/view.php?id=" . $course->id . "#section-" . $section->id . "'>$section_name</a></span>";
        $html .= "</h3>"; //h31 close
        $html .= "<ul class='section img-text'>"; //ul2

        foreach ($resources as $resource) {

            if ($resource->section == $section->section) {

                $html .= "<li class='activity resource modtype_resource' id='module-" . $resource->cm . "'>"; //li2
                $html .= "<div class='mod-indent'></div>";
                $html .= "<a href='" . $CFG->wwwroot . "/mod/resource/view.php?id=" . $resource->cm . "'>"; //a1
                $html .= "<img src='" . $CFG->wwwroot . "/pix/" . $resource->icon . "' class='iconlarge activityicon' role='presentation'>";
                $html .= "<span class='instancename'>$resource->name</span>";
                $html .= "</a>"; //a1 close

                // ratings
                $html .= "<span style='float: right;' class='resource-ratings' id='resource-" . $resource->id . "'>";

                $html .="
<form id=\"ratingForm\" method='post'>
    <fieldset class=\"rating\">
        <input type=\"radio\" id=\"star5\" name=\"rating\" value=\"5\" /><label for=\"star5\" title=\"Rocks!\">5 stars</label>
        <input type=\"radio\" id=\"star4\" name=\"rating\" value=\"4\" /><label for=\"star4\" title=\"Pretty good\">4 stars</label>
        <input type=\"radio\" id=\"star3\" name=\"rating\" value=\"3\" /><label for=\"star3\" title=\"Meh\">3 stars</label>
        <input type=\"radio\" id=\"star2\" name=\"rating\" value=\"2\" /><label for=\"star2\" title=\"Kinda bad\">2 stars</label>
        <input type=\"radio\" id=\"star1\" name=\"rating\" value=\"1\" /><label for=\"star1\" title=\"Sucks big time\">1 star</label>
    </fieldset>
    <i class=\"fa fa-paper-plane\" aria-hidden=\"true\"></i>
</form>";

                $html .= "</span>";
                // ratings

                $html .= "<br>";

                // comments
                $args = new stdClass;
                $args->context   = $PAGE->context;
                $args->course    = $course;
                $args->area      = 'resource_comments';
                $args->itemid    = $resource->id;
                $args->component = 'block_commentsandratings';
                $args->linktext  = get_string('showcomments');
                $args->showcount = true;

                $comment = new comment($args);
                $comment->set_view_permission(true);
                $comment->set_fullwidth();

                $html .= "<div style='margin-top: 10px; margin-left: 20px;'>";
                $html .= $comment->output(true);
                $html .= "</div>";
                // comments

                $html .= "</li>"; //li2 close
            }
        }

        $html .= "</ul>"; //ul2 close
        $html .= "</div>"; //div1 close
        $html .= "</li>"; //li1 close
    }
}

$html .= "</ul>"; //ul1 close
$html .= "</div>";

$html .= "

<style>

.fa-paper-plane {
    margin-top: 12px;
}
.fa-paper-plane:hover {
    cursor: pointer;
    color: #00acdf;
}

.rating {
    float:left;
    margin-right:10px;
}

.rating:not(:checked) > input {
    position:absolute;
    top:-9999px;
    clip:rect(0,0,0,0);
}

.rating:not(:checked) > label {
    float:right;
    width:1em;
    padding:0 .1em;
    overflow:hidden;
    white-space:nowrap;
    cursor:pointer;
    font-size:200%;
    line-height:1.2;
    color:#ddd;
    text-shadow:1px 1px #bbb, 2px 2px #666, .1em .1em .2em rgba(0,0,0,.5);
}

.rating:not(:checked) > label:before {
    content: '★ ';
}

.rating > input:checked ~ label {
    color: #f70;
    text-shadow:1px 1px #c60, 2px 2px #940, .1em .1em .2em rgba(0,0,0,.5);
}

.rating:not(:checked) > label:hover,
.rating:not(:checked) > label:hover ~ label {
    color: gold;
    text-shadow:1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0,0,0,.5);
}

.rating > input:checked + label:hover,
.rating > input:checked + label:hover ~ label,
.rating > input:checked ~ label:hover,
.rating > input:checked ~ label:hover ~ label,
.rating > label:hover ~ input:checked ~ label {
    color: #ea0;
    text-shadow:1px 1px goldenrod, 2px 2px #B57340, .1em .1em .2em rgba(0,0,0,.5);
}

.rating > label:active {
    position:relative;
    top:2px;
    left:2px;
}

.clearfix:before,
.clearfix:after {
    content: \" \"; /* 1 */
    display: table; /* 2 */
}

.clearfix:after {
    clear: both;
}

.clearfix {
    *zoom: 1;
}

</style>

";

echo $html;
echo $OUTPUT->footer();

$javascript = "
<script type=\"text/javascript\">

require(['jquery'], function($) {
    
    
    $('.fa-paper-plane').click(function() {

        // ajax add rating            
        
    });
    
});

    
</script>

";

echo $javascript;