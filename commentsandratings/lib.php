<?php
/**
 * Created by PhpStorm.
 * User: chevu
 * Time: 7:33 PM
 */

function block_commentsandratings_comment_validate($comment_param) {
    if ($comment_param->commentarea != 'resource_comments') {
        throw new comment_exception('invalidcommentarea');
    }
    return true;
}

/**
 * Running addtional permission check on plugins
 *
 * @package  block_comments
 * @category comment
 *
 * @param stdClass $args
 * @return array
 */
function block_commentsandratings_comment_permissions($args) {
    return array('post'=>true, 'view'=>true);
}

function commentsandratings_supports($feature) {
    switch($feature) {
        case FEATURE_RATE: return true;
        default: return null;
    }
}

function commentsandratings_rating_permissions($context) {
    return array(
        'view'=> true,
        'viewany' => true,
        'viewall' => true,
        'rate' => true
    );
}

function commentsandratings_rating_validate($params) {
    if (!array_key_exists('itemid', $params) || !array_key_exists('context', $params) || !array_key_exists('rateduserid', $params)) {
        throw new rating_exception('missingparameter');
    }
    return true;
}