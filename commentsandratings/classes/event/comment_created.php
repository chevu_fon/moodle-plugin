<?php
/**
 * Created by PhpStorm.
 * User: chevu
 * Date: 9/26/2017
 * Time: 7:35 PM
 */

namespace block_commentsandratings\event;
defined('MOODLE_INTERNAL') || die();
/**
 * block_commentsandratings comment created event.
 *
 * @package    block_commentsandratings
 * @since      Moodle 3.3
 * @copyright  2017 Vuk Nikolic <nikolicm.vuk@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class comment_created extends \core\event\comment_created {
    // No need to override any method.
}