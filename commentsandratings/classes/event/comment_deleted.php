<?php
/**
 * Created by PhpStorm.
 * User: chevu
 * Time: 7:35 PM
 */



namespace block_comments\event;
defined('MOODLE_INTERNAL') || die();
/**
 * block_commentsandratings comment deleted event.
 *
 * @package    block_commentsandratings
 * @since      Moodle 3.3
 * @copyright  2017 Vuk Nikolic <nikolicm.vuk@gmail.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class comment_deleted extends \core\event\comment_deleted {
    // No need to override any method.
}